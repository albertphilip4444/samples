import {Routes,Route} from 'react-router-dom';
import './App.css';
import Signin from './pages/Signin.js';
import Admin_panel from './pages/Admin_panel.js';
import User_Management from './pages/User_Management.js';



function App() {
  return (
    <div >
      <Routes>

  
        <Route path='/' element={<Signin/>}/>
        <Route path='/admin' element={<Admin_panel/>}/>
        <Route path='/user-management' element={<User_Management/>}/>



     
      </Routes>
   
    </div>
  );
}

export default App;
