import React from 'react';
import Logo from '../images/logo.png';
import {NavLink} from 'react-router-dom';
import { useState,useEffect } from 'react';
import { Modal } from 'antd';


// function Navbar() {



//   return (
  
//     <div style={{fontFamily:'Montserrat, sans-serif'}}>
//        <div className='h-[70px] bg-white flex items-center relative    '>
//         <div className='w-[79px] h-[45px] ml-[48px]'>
//             <img  src={Logo} alt=''/>
//         </div>
//         <div className='ml-[200px] flex justify-between 
//                         max-md:ml-[80px] 
//                         max-sm:ml-[27px]     '>
//              <NavLink to="/admin" >
//             <h5 className='font-bold text-[14px] text-[#CC9306] max-sm:text-[11px]  text-center  ' >Sales Report</h5>
//             </NavLink>   

//             <NavLink to="/user-management" >
//             <h5 className='font-bold text-[14px] text-[#7C7C7C] max-sm:text-[11px] text-center ml-[32px] ' >User Management</h5>
//             </NavLink>    
       
//         </div>
//         <div className='absolute right-[50px] max-sm:right-[30px] bg-[#F4CF7B] w-[48px] h-[48px] rounded-[50%] flex items-center justify-center'>
//             <h3 className='font-bold text-[16px]  text-[#936805]'>D</h3>
//         </div>


//        </div>
//     </div>
   
//   )
// }

const Navbar = ({ activePage }) => {


  const [screenWidth, setScreenWidth] = useState(window.innerWidth);
  useEffect(() => {
    const handleResize = () => {
      setScreenWidth(window.innerWidth);
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);


  //profile modal
  const [visible,setVisible ] = useState(false);

  return (
    <div>
    <div className='fixed top-0 left-0 right-0'  style={{ fontFamily: 'Montserrat, sans-serif ', zIndex:'1000' }}>
      
      <div className='h-[70px] bg-white flex items-center relative w-auto  '>
        <div className='w-[79px] h-[45px] ml-[48px] 
                        max-sm:ml-[20px]'>
          <img src={Logo} alt=''/>
        </div>
        {screenWidth < 500 ?

        <div className='fixed bottom-0 left-0 right-0'  style={{ fontFamily: 'Montserrat, sans-serif ', zIndex:'1000' }}>
      
        <div className='h-[70px] bg-[#ffffff] flex items-center justify-between relative w-auto '>

          <div className='  flex justify-between max-md:ml-[80px] max-sm:mx-[20px] max-sm:mt-[5px] w-[100%]'>
            <NavLink to="/admin">
              <h5 className={`font-bold text-[14px] ${activePage === 'admin' ? 'text-[#CC9306] font-bold '  : 'text-[#7C7C7C]'} max-sm:text-[11px] font-[250] text-center`}  >Sales Report</h5>
            </NavLink>
            <h6 className='ml-[67px] text-[#45464477]'>|</h6>
            <NavLink to="/user-management">
              <h5 className={`font-bold text-[14px] ${activePage === 'user-management' ? 'text-[#CC9306] font-bold'  : 'text-[#7C7C7C]'} max-sm:text-[11px] font-[250] text-center ml-[32px]`}>User Management</h5>
            </NavLink>
          </div>

        </div>
     
      </div>
        

        :

        <div className='ml-[200px] flex justify-between max-md:ml-[80px] max-sm:ml-[27px]'>
          <NavLink to="/admin">
            <h5 className={`text-[14px] ${activePage === 'admin' ? 'text-[#CC9306] font-bold  '  : 'text-[#7C7C7C]'} max-sm:text-[11px] font-[500] text-center`}  >Sales Report</h5>
          </NavLink>
          <NavLink to="/user-management">
            <h5 className={` text-[14px] ${activePage === 'user-management' ? 'text-[#CC9306] font-bold'  : 'text-[#7C7C7C]'} max-sm:text-[11px] font-[500] text-center ml-[32px]`}>User Management</h5>
          </NavLink>
        </div>
}
       
        <div  className='absolute right-[50px] max-sm:right-[20px] bg-[#F4CF7B] w-[48px] h-[48px] rounded-[50%] flex items-center justify-center
                         max-sm:h-[40px]  max-sm:w-[40px] cursor-pointer ' 
                         onClick={()=>{setVisible(true)}}>
          <h3 className='font-bold text-[16px] text-[#936805]'>D</h3>
        </div>
       
        <Modal onCancel = {()=> setVisible(false)} 
    footer={null} 
    visible={visible} 
    // style={{ top: 70, right:0, }}
    style={{
      alignSelf: 'flex-start',
      marginRight: '20px',
      marginTop: '-27px',
     
    }}
    width={278}
    closable={false}
    
    >
      <div className='w-[214px] h-[114px] mx-[8px] my-[6px]' style={{ fontFamily: 'Montserrat, sans-serif '}}>
        <div className='w-auto h-[45px]  '>
         <h5 className='font-[600] text-[16px] leading-[19.5px]'>Profile</h5>
         <h6 className='font-[500] text-[14px] leading-[17.07px] mt-[10px]'>Doe James</h6>
         </div>
        <button className="bg-[#F2F2F2] w-[100%] h-[37px] mt-[32px] hover:bg-[#d8d6d6] text-[#575757] font-[600] text-[14px] py-2 px-4  rounded">Log out</button>
      </div>
      
   
    </Modal>
   
      </div>
     
    </div>
    
    </div>
  );
};

export default Navbar
