import React from 'react';
import Navbar from '../components/Navbar.js';
import { useState,useEffect } from 'react';
import { Modal } from 'antd';
import Select from 'react-select';
import Eye from '../images/icon.png'



function User_Management() {

    
  const [screenWidth, setScreenWidth] = useState(window.innerWidth);
  useEffect(() => {
    const handleResize = () => {
      setScreenWidth(window.innerWidth);
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  //Add new user
const [visible,setVisible ] = useState(false);

//multi selection 
const [selectedOptions, setSelectedOptions] = useState([]);

  const options = [
    { value: 'Option 1', label: 'Option 1' },
    { value: 'Option 2', label: 'Option 2' },
    { value: 'Option 3', label: 'Option 3' },
    // Add more options as needed
  ];

  const handleChange = (selectedValues) => {
    setSelectedOptions(selectedValues);
  };

  const customStyles = {
    control: (provided,state) => ({
      ...provided,
      backgroundColor: '#FAFAFA',
      border: state.isFocused ? '1px solid #E3E3E3' : '1px solid #ced4da',
      boxShadow: state.isFocused ? '0 0 0 0' : null,
      '&:hover': {
        border: state.isFocused ? '1px solid #E3E3E3' : '1px solid #ced4da'
      }
    }),
    multiValue: (provided) => ({
        ...provided,
        backgroundColor: '#FBECCA',
        border:'1px solid #A97604',
      
      }),
      multiValueLabel: (provided) => ({
        ...provided,
        color: '#C08600',
      }),
      option: (provided, state) => ({
        ...provided,
        backgroundColor: state.isFocused ? '#FBECCA' : null,
        '&:hover': {
          backgroundColor: '#FBECCA',
        },
      }),  
      placeholder: (provided) => ({
        ...provided,
        padding: '8px 5px', // Adjust the padding value as needed
      }),

      indicatorSeparator: () => ({
        display: 'none',
      }),
      dropdownIndicator: (provided) => ({
        ...provided,
        color: '#505967',
        width: '31px',
      
        
      }),
  };
  

  //edit table
const [edit,setEdit ] = useState(false);

  return (
    <div>
        <Navbar activePage="user-management" />
   <div className='bg-[#f8f8f9] px-[48px] pb-[80px]   pt-[86px]
                  max-sm:px-[20px]  ' style={{fontFamily:'Montserrat, sans-serif'}} >

      <div className='w-auto h-auto '>
        <div className=' flex justify-between'>
         <h1 className='font-[600] text-[24px] leading-[29.26px] text-[#343434]
                         max-sm:text-[20px]'>Manage Users</h1>

      {screenWidth < 756 ?
      <button className="bg-[#F4CF7B] hover:bg-[#e7ca88] text-[#7A4310] font-[600] text-[14px]  px-4 border border-[#F4CF7B] rounded mr-3 "
      onClick={()=>{setVisible(true)}}>
      
       <span className="material-symbols-outlined h-[45px] pt-2">person_add</span>
       </button>
      :
         <button className="bg-[#F4CF7B] hover:bg-[#dbba6c] h-[48px] w-[163px] text-[#7A4310] font-[600] text-[14px] py-2 px-4  rounded"
         onClick={()=>{setVisible(true)}}>Add New User</button>
      }
        </div>

        <div className='mt-[24px]'>
        <div className= ' bottom-div-with-hidden-scrollbar h-auto   overflow-x-auto' >
  

        <table className="w-full text-sm text-left rtl:text-right">
      <thead className="bg-[#FFFAEE] text-[#393939] text-[14px] font-[500] h-[67px] leading-[16.94px] max-sm:text-[12px]">
        <tr>
          <th scope="col" className="px-6 py-3">
            User Name
          </th>
          <th scope="col" className="px-6 py-3">
            User E-mail
          </th>
          <th scope="col" className="px-6 py-3">
            Role
          </th>
          <th scope="col" className="px-[45px] py-3">
          Action
          </th>
         
        </tr>
      </thead>
      <tbody className='h-[70px] font-[500] text-[14px] leading-[19.36px] text-[#464646] max-sm:text-[12px]'  style={{fontFamily:'Montserrat, sans-serif'}}>
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
        <th scope="row" className="px-6 py-4 font-medium   w-[173px]  ">
            {screenWidth < 756 ?
             <div className='flex  items-center'>
            
             John Doe
             </div>
             :
            <div className='flex  items-center'>
        <div className='  bg-[#F0F2EC] w-[32px] h-[32px] rounded-[50%] flex items-center justify-center mr-[6px]'>
            <h3 className='font-[500] text-[14px]  text-[#828282]'>J</h3>
        </div>
        John Doe
        </div>
        }

         </th>
         <td className="px-6 py-4 text-[#050505] w-[163px] ">
           johndeo124@gmail.com 
         </td>
          <td className="px-6 py-4  text-[#050505]">
          Representative
          </td>
         
  
          <td className="px-[33px] py-4  ">
              <div className=' w-[70px] flex justify-between max-sm:w-[60px] max-sm:ml-[5px]'>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px] "  type="button" onClick={()=>{setEdit(true)}} > border_color</span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px] " type="button"> delete</span>
     
              </div>
          </td>
  
        </tr>
        
  
      
  
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
        <th scope="row" className="px-6 py-4 font-medium   w-[173px]  ">
            {screenWidth < 756 ?
             <div className='flex  items-center'>
            
             John Doe
             </div>
             :
            <div className='flex  items-center'>
        <div className='  bg-[#F0F2EC] w-[32px] h-[32px] rounded-[50%] flex items-center justify-center mr-[6px]'>
            <h3 className='font-[500] text-[14px]  text-[#828282]'>J</h3>
        </div>
        John Doe
        </div>
        }

         </th>
         <td className="px-6 py-4 text-[#050505] w-[163px] ">
           johndeo124@gmail.com 
         </td>
          <td className="px-6 py-4  text-[#050505]">
          Representative
          </td>
         
  
          <td className="px-[33px] py-4  ">
          <div className=' w-[70px] flex justify-between max-sm:w-[60px] max-sm:ml-[5px]'>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px] "  type="button" onClick={()=>{setEdit(true)}} > border_color</span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px] " type="button"> delete</span>
     
              </div>
          </td>
  
        </tr>
  
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
        <th scope="row" className="px-6 py-4 font-medium   w-[173px]  ">
            {screenWidth < 756 ?
             <div className='flex  items-center'>
            
             John Doe
             </div>
             :
            <div className='flex  items-center'>
        <div className='  bg-[#F0F2EC] w-[32px] h-[32px] rounded-[50%] flex items-center justify-center mr-[6px]'>
            <h3 className='font-[500] text-[14px]  text-[#828282]'>J</h3>
        </div>
        John Doe
        </div>
        }

         </th>
         <td className="px-6 py-4 text-[#050505] w-[163px] ">
           johndeo124@gmail.com 
         </td>
          <td className="px-6 py-4  text-[#050505]">
          Representative
          </td>
         
  
          <td className="px-[33px] py-4  ">
          <div className=' w-[70px] flex justify-between max-sm:w-[60px] max-sm:ml-[5px]'>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px] "  type="button" onClick={()=>{setEdit(true)}} > border_color</span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px] " type="button"> delete</span>
     
              </div>
          </td>
  
        </tr>
      </tbody>
    </table>
    </div>
        </div>

       
      
      </div>
      <Modal onCancel = {()=> setVisible(false)} 
    footer={null} 
    visible={visible}
    width={460}
    
    top={0}>
         <div className='pt-[20px] py-[24px]'style={{fontFamily:'Montserrat, sans-serif'}}>
        <div className='w-[auto] '>
            <h1 className='font-[600] text-[20px] leading-[24.38px] text-[#1C1C1C] mb-[24px]'>Add New User</h1>
            <hr></hr>

            {/* --------form------------ */}
            <div className='w-[auto] '>
            <form >
                    <div className="mb-4 mt-6">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            User Name
                        </label>
                        <input className=" bg-[#FAFAFA] border border-[#E3E3E3] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder='Eg: John Doe'  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            User E-mail
                        </label>
                        <input className=" bg-[#FAFAFA] border border-[#E3E3E3] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder='Eg: Johndoe@gmail.com'  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Password
                        </label>
                        <input className=" bg-[#FAFAFA] border border-[#E3E3E3] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="password"  />
                        </div>
                        <div className="mb-4 relative">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Re-enter Password
                        </label>
                        <input className=" bg-[#FAFAFA]  border border-[#E3E3E3] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="password"  />
                        <button>
                    
                      <img className='absolute w-[16px] right-3 bottom-3' src={Eye} alt=''/>
                      </button>
                        </div>

                        <div className='mt-5'>
                            <h4 className='font-[600] text-[18px] leading-[22px] text-[#32324D]'>Roles</h4>
                            <div className="mb-4 mt-5">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            User Roles
                        </label>
                        <div className='bg-[#FAFAFA]  border border-[#E3E3E3]  rounded w-full h-[40px] py-2 pl-3 pr-2'>
                          <select id="paid"  className=" bg-[#FAFAFA]  w-full   text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  >
                          <option  value="none"  className='my-5' selected disabled hidden>Choose Role</option>

                            <option  className='my-5'>Name1</option>
                            <option >Name2</option>
                           
                          </select>
                          </div>

                        </div>

                        <div className="mb-4 mt-5">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                         Assign to manager
                        </label>
                        <div className='bg-[#FAFAFA]  border border-[#E3E3E3]  rounded w-full h-[40px] py-2 pl-3 pr-2'>
                          <select id="paid"  className=" bg-[#FAFAFA]  w-full   text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  >
                         <option  value="none" selected disabled hidden>Choose ...</option>

                            <option  className='m-5'>Name1</option>
                            <option >Name2</option>
                           
                          </select>
                          </div>

                        </div>

                        <div className="mb-4 mt-5">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            District
                        </label>
                          {/* <select id="paid" name="carlist" className="  bg-[#FAFAFA] border border-[#E3E3E3] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  >
                         <option value="none" selected disabled hidden>Choose District</option>

                            <option  className='m-5'>Name1</option>
                            <option >Name2</option>
                           
                          </select> */}
                       
                          <div >
      <Select
        isMulti
        value={selectedOptions}
        options={options}
        onChange={handleChange}
        styles={customStyles}
        placeholder="Choose a District"
      />



     
    </div>
                          
                        
                        

                        </div>
                        </div>


                      
                       

                        <div className='mt-[40px]  h-[49px] flex items-center relative'>
                            
                            <div className='absolute right-0 w-[203] h-[49px]   '>
                            <button className="bg-[#F6F6F6] hover:bg-[#fcfaf7] text-black font-[400] text-[12px] py-4 px-4 border border-[#EBEBEB] rounded mr-3 mt-0 h-[48px] w-[163px] 
                            max-sm:w-[100px] ">Cancel</button>
                           
                           
                            <button className="bg-[#F4CF7B] hover:bg-[#e7ca88] text-[#7A4310] font-[600] text-[14px] py-2 px-4 border border-[#F4CF7B] w-[163px] h-[48px] rounded
                            max-sm:w-[100px] ">Save</button>


                            </div>
                        </div>
 
                    </form>
            </div>

            {/* --------form  end------------ */}
        </div>
    </div>
    </Modal>



    {/*-----table edit-----modal  */}
    <Modal onCancel = {()=> setEdit(false)} 
    footer={null} 
    visible={edit}
    width={460}
    top={0}>
     
     <div className='pt-[20px] py-[24px]'style={{fontFamily:'Montserrat, sans-serif'}}>
        <div className='w-[auto] '>
            <h1 className='font-[600] text-[20px] leading-[24.38px] text-[#1C1C1C] mb-[24px]'>Edit User</h1>
            <hr></hr>

            {/* --------form------------ */}
            <div className='w-[auto] '>
            <form >
                    <div className="mb-4 mt-6">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            User Name
                        </label>
                        <input className=" bg-[#FAFAFA] border border-[#E3E3E3] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder='Eg: John Doe'  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            User E-mail
                        </label>
                        <input className=" bg-[#FAFAFA] border border-[#E3E3E3] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder='Eg: Johndoe@gmail.com'  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Password
                        </label>
                        <input className=" bg-[#FAFAFA] border border-[#E3E3E3] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="password"  />
                        </div>
                        <div className="mb-4 relative">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Re-enter Password
                        </label>
                        <input className=" bg-[#FAFAFA] border border-[#E3E3E3] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="password"  />
                        <button>
                    
                    <img className='absolute w-[16px] right-3 bottom-3.5' src={Eye} alt=''/>
                    </button>
                        </div>

                        <div className='mt-5'>
                            <h4 className='font-[600] text-[18px] leading-[22px] text-[#32324D]'>Roles</h4>
                            <div className="mb-4 mt-5">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            User Roles
                        </label>
                        <div className='bg-[#FAFAFA]  border border-[#E3E3E3]  rounded w-full h-[40px] py-2 pl-3 pr-2'>
                          <select id="paid"  className=" bg-[#FAFAFA]  w-full   text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  >
                          <option  value="none"  className='my-5' selected disabled hidden>Choose Role</option>

                            <option  className='my-5'>Name1</option>
                            <option >Name2</option>
                           
                          </select>
                          </div>

                        </div>

                        <div className="mb-4 mt-5">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                         Assign to manager
                        </label>
                        <div className='bg-[#FAFAFA]  border border-[#E3E3E3]  rounded w-full h-[40px] py-2 pl-3 pr-2'>
                          <select id="paid"  className=" bg-[#FAFAFA]  w-full   text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  >
                         <option  value="none" selected disabled hidden>Choose ...</option>

                            <option  className='m-5'>Name1</option>
                            <option >Name2</option>
                           
                          </select>
                          </div>

                        </div>

                        <div className="mb-4 mt-5">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            District
                        </label>
                          {/* <select id="paid" name="carlist" className="  bg-[#FAFAFA] border border-[#E3E3E3] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  >
                         <option value="none" selected disabled hidden>Choose District</option>

                            <option  className='m-5'>Name1</option>
                            <option >Name2</option>
                           
                          </select> */}
                       
                          <div >
      <Select
        isMulti
        value={selectedOptions}
        options={options}
        onChange={handleChange}
        styles={customStyles}
        placeholder="Choose a District"
      />



     
    </div>
                          
                        
                        

                        </div>
                        </div>


                      
                       

                        <div className='mt-[40px]  h-[49px] flex items-center relative'>
                            
                            <div className='absolute right-0 w-[203] h-[49px]   '>
                            <button className="bg-[#F6F6F6] hover:bg-[#fcfaf7] text-black font-[400] text-[12px] py-4 px-4 border border-[#EBEBEB] rounded mr-3 mt-0 h-[48px] w-[163px] 
                             max-sm:w-[100px] ">Cancel</button>
                           
                           
                            <button className="bg-[#F4CF7B] hover:bg-[#e7ca88] text-[#7A4310] font-[600] text-[14px] py-2 px-4 border border-[#F4CF7B] w-[163px] h-[48px] rounded 
                             max-sm:w-[140px]">Save Changes</button>


                            </div>
                        </div>
 
                    </form>
            </div>

            {/* --------form  end------------ */}
        </div>
    </div>




    </Modal>
   </div>
    </div>
  )
}

export default User_Management
