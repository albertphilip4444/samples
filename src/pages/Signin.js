import React from 'react';
import Logo from '../images/logo.png';
import {Link} from 'react-router-dom';
import { useState,useEffect } from 'react';
import Eye from '../images/icon.png';
import Img1 from '../images/sign2.png';
import Img2 from '../images/sign1.png';
import Img3 from '../images/sign3.png';


function Signin() {

    const [screenWidth, setScreenWidth] = useState(window.innerWidth);
    useEffect(() => {
      const handleResize = () => {
        setScreenWidth(window.innerWidth);
      };
  
      window.addEventListener('resize', handleResize);
  
      return () => {
        window.removeEventListener('resize', handleResize);
      };
    }, []);
    
  return (
    <div>
    <div className='h-[100vh] bg-[#FFFCEF] relative flex items-center justify-center max-sm:bg-[#ffff]'>
        <div className='absolute  w-[505px] h-[499.5px] rounded-2xl bg-[#ffff] max-sm:w-[375px]  grid grid-rows-[140.5px,1fr] p-[32px] gap-y-[16px] max-sm:mt-[-120px]'>
            <div className=' max-sm:w-[310px]' >
                <div className='  flex  justify-center' >
                <img className='w-[118.5px] h-[67.5px] max-sm:w-[100px] max-sm:object-contain' src={Logo} alt=''/>
                </div>
                <div className='h-[57px] mt-[8px]'>
                    <h4 className='text-center font-[700] text-[24px] text-[#32324D]
                                   max-sm:text-[20px]' style={{fontFamily:'Montserrat, sans-serif'}}>Welcome </h4>
                    <h6 className='text-center font-[500] text-[16px] text-[#666687] 
                                     max-sm:text-[12px]' style={{fontFamily:'Montserrat, sans-serif'}} >Log in to your Viventure account</h6>

                    
                </div>

            </div>
            <div className='   max-sm:w-[310px]'>
                <form >
                <div className='h-[162px] w-[100%]  '>
                    <div className="m-0 " >
                     <label className="block text-[#797777]  font-[500] text-[12px]" style={{fontFamily:'Montserrat, sans-serif'}} htmlFor="Email">
                        Email
                     </label>
                     <input className="w-[440px] max-sm:w-[310px] h-[48px] rounded-[8px] bg-[#F8F8F9] pl-3" id="email" type="email"  />
                    </div>
                    <div className="mt-[10px] relative">
                    <label className="block text-[#797777]  font-[500] text-[12px]" style={{fontFamily:'Montserrat, sans-serif'}} htmlFor="password">
                        Password
                      </label>
                     
                      <input className="w-[440px] h-[48px] max-sm:w-[310px] relative rounded-[8px] bg-[#F8F8F9] pl-3  " id="email" type="password"  />
                      <button>
                    
                    <img className='absolute w-[16px] right-4 bottom-4' src={Eye} alt=''/>
                    </button>
                    </div>

                </div>
                <div className='  mt-[16px]'>
                        <div className="flex items-center mb-[26px]">
                               

                                <input className='w-[20px] h-[20px] border-[#C0C0CF] rounded' type="checkbox" id="Remember_me" name="Remember_me"  />
                                <label  htmlFor="Remember_me">
                                     <h6 className='font-[400] text-[14px] leading-[20px] text-[#32324D] pl-2 '>Remember me</h6>
                                     </label>
                            

                        </div>
                   <Link to="/admin">
                    <button className='w-[440px] max-sm:w-[310px] h-[41px] bg-[#F9B916] hover:bg-[#e7c160] rounded-[4px] font-[600] text-[14px] text-[#7A6161]' >
                        Login
                        </button>
                        </Link>


                </div>
                </form>
            </div>

        
        
        
        </div>

    {screenWidth < 850 ?


       <div className='flex items-end justify-center'>
        <img className="w-[210px] object-cover absolute  bottom-0  max-md:w-[210px] max-sm:w-[250px]   " src={Img2} alt=''/>

       </div>
        
    :
    
    <div>
    <img className="w-[140px] object-cover absolute left-0 top-[350px]  " src={Img1} alt=''/>
           <img className="w-[140px] object-cover absolute right-0 bottom-[100px]  " src={Img3} alt=''/>
          <div className='flex items-end justify-center'>
           <img className="w-[210px] max-md:w-[210px] object-cover absolute  bottom-0   " src={Img2} alt=''/>
   
          </div>
           </div>
    
    }

    
       



       
    </div>

    </div>
  )
}

export default Signin
