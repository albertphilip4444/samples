import React from 'react'
import Navbar from '../components/Navbar.js';
import Hand from '../images/hand.png';
import { useState,useEffect } from 'react';
import Frame from '../images/Frame.png';
import Frame2 from '../images/Frame2.png';
import Frame3 from '../images/Calendar.png';
import Frame4 from '../images/Invoice1.png';
import Calendar_svg from '../images/Calendar_svg.svg';
import { Modal } from 'antd';
import './Styles/Admin_panel.css';


import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';





function Admin_panel() {


  const [screenWidth, setScreenWidth] = useState(window.innerWidth);
  useEffect(() => {
    const handleResize = () => {
      setScreenWidth(window.innerWidth);
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);



    


//upload new
const [visible,setVisible ] = useState(false);

//calender modal
const [showCalender,setShowCalender ] = useState(false);
// const [selectedRange, setSelectedRange] = useState({
//   startDate: new Date(),
//   endDate: new Date(),
//   key: 'selection',
// });

// const handleSelect = (ranges) => {
//   setSelectedRange(ranges.selection);
//   // Handle the selected range internally within the component
//   console.log('Selected range:', ranges.selection);
// };



const [startDate, setStartDate] = useState(null);
const [endDate, setEndDate] = useState(null);
const [showModal, setShowModal] = useState(false);

const handleSelectDate = () => {
  setShowModal(true);
};

const handleModalClose = () => {
   setShowModal(false);
   setShowCalender(false);
};

const handleDateSelect = (date) => {
  if (!startDate) {
    setStartDate(date);
  } else if (!endDate) {
    setEndDate(date);
  } else {
    setStartDate(date);
    setEndDate(null);
  }
};

const handleOkButtonClick = () => {
  setShowCalender(false);
  setShowModal(false);
  // Perform any other actions with selected dates
};

const formatSelectedDates = () => {
  if (startDate && endDate) {
    return `${startDate.toLocaleDateString()} - ${endDate.toLocaleDateString()}`;
  } else if (startDate) {
    return startDate.toLocaleDateString();
  } else {
    return 'Select Date';
  }
};



//edit table
const [edit,setEdit ] = useState(false);


//table button  --All list and outstanding--
const [activeButton, setActiveButton] = useState('btn1');

const handleButtonClick = (button) => {

  setActiveButton(button);
};



//upload file
const [fileName, setFileName] = useState('');

const handleFileChange = (e) => {
  const file = e.target.files[0];
  if (file) {
    setFileName(file.name);
  } else {
    setFileName('');
  }
};


  return (
    <div >
    
    <Navbar activePage="admin" />

   
   <div className='bg-[#f8f8f9] px-[48px] pb-[80px]   pt-[86px]
                   max-sm:px-[20px] ' style={{overflowX:'hidden'}}>
   <div className='  mb-[20px]  relative'>
    <div className='h-[127px] w-[602px]  max-lg:h-[195px] ' >
        <div className='h-[60px] w-[225px]   grid grid-cols-[40px,1fr] 
                         max-sm:grid-cols-[35px,1fr] '>
             <div className='h-[40px] w-[40px]
                              max-sm:w-[32px]   max-sm:h-[32px]   max-sm:mt-[6px]   '>
                <img src={Hand} alt=''/>
             </div>
             <div>
             <h5 className='h-[40px]  flex items-center font-[600] text-[24px] text-[#343434] 
                            max-sm:text-[16px]' >Hi, Doe</h5>
             <h6 className='font-[500] text-[16px] text-[#6D6D6D]
                             max-sm:text-[12px]'>Welcome to Viventure</h6>
             </div>

        </div>


        {/* {screenWidth < 1000 ? (
        <p className="text-xl text-blue-500">Hello</p>
      ) : (
        <p className="text-xl text-green-500">Hai all, how are you?</p>
      )} */}


{screenWidth < 756 ?
  <div className="flex justify-end items-end">
  <div className='absolute right-0 top-0 mt-4   '>

<button className="bg-[#F4CF7B] hover:bg-[#e7ca88] text-[#7A4310] font-[600] text-[14px]  px-4 border border-[#F4CF7B] rounded mr-3 "
onClick={()=>{setVisible(true)}}>

 <span className="material-symbols-outlined h-[45px] pt-2">upload</span>
 </button>
<button className="bg-black hover:bg-[#282727] text-white font-[600] text-[14px]  px-4 border border-black rounded">
<span className="material-symbols-outlined h-[45px] pt-2">download</span>
  </button>

</div>
</div>

:
<div className='absolute right-0 top-0 mt-4  '>

<button className="bg-[#F4CF7B] hover:bg-[#e7ca88] text-[#7A4310] font-[600] text-[14px] py-2 px-4 border border-[#F4CF7B] rounded mr-3 "
onClick={()=>{setVisible(true)}}>Upload New
 </button>
<button className="bg-black hover:bg-[#282727] text-white font-[600] text-[14px] py-2 px-4 border border-black rounded">Report Download</button>

</div>
}


        <div className='h-[43px] mt-[24px]    flex max-lg:flex-wrap max-lg:w-[600px] max-lg:h-[109px]  max-md:w-[400px] justify-between gap-x-[32px] max-lg:gap-x-[12px] max-lg:gap-y-[12px] 
                         max-sm:gap-x-[10px] max-sm:w-[335px] '>
           
            <div className='bg-white w-[200px] max-sm:w-[160px]  flex items-center pl-2  cursor-pointer rounded-[4px] max-lg:h-[45px] ' >
            {/* <span className="material-symbols-outlined font-[250] text-[#161616] " > calendar_month</span>
            <h5 className="text-[#161616]  w-[185px] pl-[5px]  text-center inline-flex items-center max-lg:h[43px]"  >Select a date</h5> */}
    <div className='  font-[500] text-[14px] text-[#161616] relative pl-[24px]
                       max-sm:text-[11px] max-sm:pl-[26px]  max-sm:text-[#797777] ' onClick={()=>{setShowCalender(true)}}  type='button'>
      <button onClick={handleSelectDate}  >
        <img className="w-[20px] h-[20px] inline-block absolute left-0 "src={Calendar_svg} alt=''/>
        {formatSelectedDates()}</button>
     
    </div>
            </div>
           


            {/* ------drop down button 1-------------- */}
          


       
            <div className='bg-white  w-[185px] max-sm:w-[160px]  rounded-[4px] z-10 max-lg:h-[45px]  '>
            <div className=''>
            <select id="paid" name="carlist" className="  rounded w-[170px] max-sm:w-[150px] h-[42px] py-2 px-3  font-[500] text-[14px] text-[#161616] leading-tight focus:outline-none focus:shadow-outline 
                                                         max-sm:text-[11px]  max-sm:text-[#797777] "  >
                         <option value="none" selected disabled hidden className="text-[#161616]  w-[185px] pl-[16px] py-2.5   inline-flex  ">Rep Name</option>

                            <option>Name 1</option>
                            <option>Name 2</option>
                           
                          </select>
        </div>
            </div>



            {/* ------drop down button 2-------------- */}
            <div className='bg-white  w-[185px] max-sm:w-[160px] rounded-[4px] z-10 max-lg:h-[45px]'>
            <div>
            <select id="paid" name="carlist" className="  rounded w-[170px] max-sm:w-[150px]  h-[42px] py-2 px-3 font-[500] text-[14px] text-[#161616] leading-tight focus:outline-none focus:shadow-outline 
                                                          max-sm:text-[11px]  max-sm:text-[#797777] "  >
                         <option value="none" className="text-[#161616]  w-[185px] pl-[16px] py-2.5   inline-flex " selected disabled hidden >District</option>

                            <option >District 1</option>
                            <option> District 2</option>
                           
                          </select>
        </div>
            </div>

        </div>
    </div>

   <div className='h-[auto]  mt-[32px] '>





    <div className='h-[185px]   w-auto max-lg:w-[700px] max-lg:h-[384px] max-lg:grid-cols-[1fr,1fr]  max-md:w-[550px] max-md:h-[325px]  max-sm:w-[335px] max-sm:h-[205px] max-sm:gap-x-[5px] grid grid-cols-[1fr,1fr,1fr,1fr] gap-x-[20px] max-2xl:grid-cols-[1fr,1fr,1fr] max-2xl:h-[387px] gap-y-[15px] max-2xl:w-[1060px]  max-xl:w-[995px] max-md:gap-y-[5px]'>
        <div className='bg-[#ffffff] flex  justify-start relative rounded-[8px] max-lg:w-auto max-sm:h-[97px] max-2xl:h-[185px] max-md:h-[145px] max-lg:h-[184px] '>
            <div className='w-[auto] h-[86px] max-sm:h-[50px] max-sm:w-auto max-md:h-[74px]  mt-[49px] ml-[24px] max-lg:mt-[38px] max-sm:mt-[18px] max-md:mt-[28px] max-sm:ml-[13px] relative  '>
           <h5 className='font-[500] text-[18px] max-lg:text-[16px] max-md:text-[14px] max-sm:text-[11px] text-start text-[#797777] leading-[21.94px]' style={{fontFamily:'Montserrat, sans-serif'}}  >CLOSED DEALS</h5>
           <h1 className='font-[600] text-[36px] max-lg:text-[33px]  max-md:text-[30px] max-sm:text-[20px]   text-black leading-[48.76px]  max-sm:leading-[24.38px] absolute left-0 bottom-[0px] m-0 ' style={{fontFamily:'Montserrat, sans-serif'}} >40</h1>
            </div>
            {screenWidth < 640 ?"":
            <div className='absolute  w-[127px] h-[73px]  top-[113px] right-0 max-lg:w-[120px]  max-lg:h-[100px]   max-md:w-[90px] max-md:h-[64px] max-md:top-[94px] max-md:right-[0px] max-sm:w-[71px] max-sm:h-[44px] max-sm:top-[43px] max-sm:left-[120px]   '>
                <img className='object-cover rounded-br-[8px]'  src={Frame} alt=''/>
            </div>
            }
           
        </div>
        <div className='bg-[#ffff] relative rounded-[8px] max-sm:h-[97px] max-2xl:h-[185px] max-lg:h-[184px] max-md:h-[145px]'>
        <div className='w-[auto] h-[86px] mt-[49px] ml-[24px] relative max-sm:h-[50px] max-sm:w-auto max-sm:mt-[18px] max-sm:ml-[13px] max-lg:mt-[38px] max-md:h-[74px] max-md:mt-[28px]  '>
           <h5 className='font-[500] text-[18px] max-lg:text-[16px] max-md:text-[14px] max-sm:text-[11px] text-start text-[#797777] leading-[21.94px]' style={{fontFamily:'Montserrat, sans-serif'}}  >TOTAL  AMOUNT CLOSED</h5>
           <h1 className='font-[600] text-[36px] max-lg:text-[33px] max-md:text-[30px] max-sm:text-[20px] text-black leading-[48.76px] max-sm:leading-[24.38px] absolute left-0 bottom-[0px] m-0 ' style={{fontFamily:'Montserrat, sans-serif'}} > &#8377; 200000</h1>
            </div>

            {screenWidth < 640 ?"":
            <div className='absolute w-[127px] h-[73px]  top-[113px] right-0 max-lg:w-[120px]  max-lg:h-[100px]    max-md:w-[90px] max-md:h-[64px] max-md:top-[91px] max-md:right-[0px] max-sm:w-[71px] max-sm:h-[44px] max-sm:top-[53px] max-sm:left-[119px] '>
                <img className='object-cover rounded-br-[8px]'  src={Frame2} alt=''/>
            </div>
             }
        </div>

        <div className='bg-[#ffff] relative rounded-[8px] max-sm:h-[97px] max-2xl:h-[185px] max-lg:h-[184px] max-md:h-[145px]'>
        <div className='  w-[auto] h-[86px] mt-[49px] ml-[24px] relative max-sm:h-[50px] max-sm:w-auto max-sm:mt-[18px] max-sm:ml-[13px] max-lg:mt-[38px] max-md:h-[74px] max-md:mt-[28px]  '>
           <h5 className='font-[500] text-[18px] max-lg:text-[16px] max-md:text-[14px] max-sm:text-[11px] text-start text-[#797777] leading-[21.94px]' style={{fontFamily:'Montserrat, sans-serif'}}  >OUTSTANDING</h5>
           <h1 className='font-[600] text-[36px] max-lg:text-[33px] max-md:text-[30px] max-sm:text-[20px] text-black leading-[48.76px] max-sm:leading-[24.38px] absolute left-0 bottom-[0px] m-0 ' style={{fontFamily:'Montserrat, sans-serif'}} >14</h1>
            </div>
            <div className=' w-[auto] h-[auto] mt-[8px] ml-[24px]
                             max-sm:ml-[13px] max-sm:mt-[1px] '>

            <h6 className='font-[500] text-[16px] max-md:text-[14px] leading-[19.5px] text-[#A7801A]
                           max-sm:text-[12px]' >Amount- &#8377; 41897</h6>
            </div>

            {screenWidth < 640 ?"":
            <div className='  absolute w-[78px] h-[73px]  top-[121px] right-0 max-lg:w-[82px]  max-lg:h-[82px] max-lg:top-[115px]    max-md:w-[70px] max-md:h-[64px] max-md:top-[87px] max-md:right-[0px] max-sm:w-[71px] max-sm:h-[44px] max-sm:top-[53px] max-sm:left-[119px] '>
                <img className='object-cover rounded-br-[8px]'  src={Frame3} alt=''/>
            </div>
            }
        </div>

        <div className='bg-[#ffff] relative rounded-[8px] max-sm:h-[97px] max-2xl:h-[185px] max-lg:h-[184px] max-md:h-[145px]'>
        <div className='w-[auto] h-[86px] mt-[49px] ml-[24px] relative max-sm:h-[50px] max-sm:w-auto max-sm:mt-[18px] max-sm:ml-[13px] max-lg:mt-[38px] max-md:h-[74px] max-md:mt-[28px]  '>
           <h5 className='font-[500] text-[18px] max-lg:text-[16px] max-md:text-[14px] max-sm:text-[11px] text-start text-[#797777] leading-[21.94px]' style={{fontFamily:'Montserrat, sans-serif'}}  >OVERDUE</h5>
           <h1 className='font-[600] text-[36px] max-lg:text-[33px] max-md:text-[30px] max-sm:text-[20px] text-black leading-[48.76px] max-sm:leading-[24.38px] absolute left-0 bottom-[0px] m-0 ' style={{fontFamily:'Montserrat, sans-serif'}} >2</h1>
            </div>
                  <div className=' w-[auto] h-[auto] mt-[8px] ml-[24px]
                                  max-sm:ml-[13px] max-sm:mt-[1px] '>
                <h6 className='font-[500] text-[16px] leading-[19.5px] max-md:text-[14px] text-[#E62424]
                                max-sm:text-[12px]' >Amount- &#8377; 1897</h6>
                </div>
                {screenWidth < 640 ?"":
            <div className='absolute w-[103px] h-[154px] top-[110px] right-0 max-lg:w-[100px]  max-lg:h-[100px]   max-lg:top[90px]  max-md:h-[64px] max-md:top-[71px] max-md:right-[0px] max-sm:w-[71px] max-sm:h-[44px] max-sm:top-[53px] max-sm:left-[119px] '>
                <img className='object-cover rounded-br-[8px]'  src={Frame4} alt=''/>
            </div>
                }
        </div>

    </div>


{/* --------table view condition */}
<div className=' '>
<div className="card-header">
        <div className="btn-group flex justify-between mt-[24px]  h-[20px] w-[160px]" role="group" aria-label="Button group">
          <button
            type="button" 
            className={`btn ${activeButton === 'btn1' ? 'text-[#C68A02] font-[600] text-[16px] max-sm:text-[14px]' : 'text-[#BFBFBF] font-[400] text-[16px] max-sm:text-[14px] '} `} 
            onClick={() => handleButtonClick('btn1')}
          >
            All List
          </button>
          <button
            type="button"
            className={`btn ${activeButton === 'btn2' ? 'text-[#C68A02] font-[600] text-[16px] max-sm:text-[14px] ' : 'text-[#BFBFBF] font-[400] text-[16px] max-sm:text-[14px] '}  `}
            onClick={() => handleButtonClick('btn2')}
          >
            Outstanding
          </button>
        </div>
      </div>
      <div className="">
        {activeButton === 'btn1' && (
          <div>

              {/* ---- All List table------------- */}
     <div className= ' bottom-div-with-hidden-scrollbar h-auto  mt-[24px]  overflow-x-auto' >
  

  <div className="relative ">
    <table className="w-full text-sm text-left rtl:text-right ">
      <thead className="bg-[#FFFAEE] text-[#393939] text-[14px] font-[500] h-[67px] leading-[16.94px] max-sm:text-[12px]">
        <tr>
          <th scope="col" className="px-6 py-3">
            Client name
          </th>
          <th scope="col" className="px-6 py-3">
            Rep Name
          </th>
          <th scope="col" className="px-6 py-3">
            Invoice No:
          </th>
          <th scope="col" className="px-6 py-3">
            Invoice Date
          </th>
          <th scope="col" className="px-6 py-3">
            PTR amount
          </th>
          <th scope="col" className="px-6 py-3">
            Trade Disc
          </th>
          <th scope="col" className="px-6 py-3">
            GST + Cess
          </th>
          <th scope="col" className="px-6 py-3">
          Total
          </th>
          <th scope="col" className="px-[35px] py-3">
          Status
          </th>
          <th scope="col" className="px-[45px] py-3">
          Action
          </th>
        </tr>
      </thead>
      <tbody className='h-[70px] font-[500] text-[16px] leading-[19.36px] text-[#464646] max-sm:text-[12px]'  style={{fontFamily:'Montserrat, sans-serif'}}>
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
        <th scope="row" className="px-6 py-4 font-medium   w-[173px]  ">
        MPM Pharma

         </th>
         <td className="px-6 py-4 text-[#050505] w-[163px] ">
           John Doe 
         </td>
          <td className="px-6 py-4  text-[#050505]">
            #12
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            29-01-24
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 1245
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            0.00
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 10
          </td>
          <td className="px-6 py-4  text-[#3C3C3C] font-[600]">
          &#8377; 1255
          </td>
          <td className="px-[28px] py-4  text-[#3C3C3C]">
            <div className='w-[65px] bg-[#DCFFC1] px-[16px] py-[4px] rounded-[43px]'>
              <h5 className='text-center font-[600] text-[14px] text-[#01570F] max-sm:text-[12px]'>Paid</h5>
            </div>
          </td>
  
          <td className="px-6 py-4  ">
          <div className=' w-[95px] flex justify-between max-sm:w-[85px] max-sm:ml-[5px] '>
              <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]"  type="button" onClick={()=>{setEdit(true)}} > border_color </span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button"> delete</span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button">download</span>
              </div>
          </td>
  
        </tr>
        
  
      
  
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 ">
          <th scope="row" className="px-6 py-4 font-medium   w-[173px]  ">
           

            Sree Narayana Ayurveda 
            
          </th>
          <td className="px-6 py-4 text-[#050505] w-[163px] ">
            John Doe mark
          </td>
          <td className="px-6 py-4  text-[#050505]">
            #12
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            29-01-24
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 1245
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            0.00
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 10
          </td>
          <td className="px-6 py-4  text-[#3C3C3C] font-[600]">
          &#8377; 1255
          </td>
          <td className="px-[28px] py-4  text-[#3C3C3C]">
            <div className='w-[65px] bg-[#DCFFC1] px-[16px] py-[4px] rounded-[43px]'>
              <h5 className='text-center font-[600] text-[14px] max-sm:text-[12px] text-[#01570F]'>Paid</h5>
            </div>
          </td>
  
          <td className="px-6 py-4  ">
          <div className=' w-[95px] flex justify-between max-sm:w-[85px] max-sm:ml-[5px] '>
              <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]"  type="button" onClick={()=>{setEdit(true)}} > border_color </span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button"> delete</span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button">download</span>
              </div>
          </td>
  
        </tr>
  
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
        <th scope="row" className="px-6 py-4 font-medium   w-[173px]  ">
        MPM Pharma

         </th>
         <td className="px-6 py-4 text-[#050505] w-[163px] ">
           John Doe 
         </td>
          <td className="px-6 py-4  text-[#050505]">
            #12
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            29-01-24
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 1245
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            0.00
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 10
          </td>
          <td className="px-6 py-4  text-[#3C3C3C] font-[600]">
          &#8377; 1255
          </td>
          <td className="px-5 py-4 ]">
          <div className='w-[95px] bg-[#FFBEBE] px-[16px] py-[4px] rounded-[43px] max-sm:w-[85px]'>
              <h5 className='text-center font-[600] text-[14px] text-[#A8422B] max-sm:text-[12px]'>Unpaid</h5>
            </div>
          </td>
  
           <td className="px-6 py-4  ">
           <div className=' w-[95px] flex justify-between max-sm:w-[85px] max-sm:ml-[5px] '>
              <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]"  type="button" onClick={()=>{setEdit(true)}} > border_color </span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button"> delete</span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button">download</span>
              </div>
          </td>
  
        </tr>
  
          <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
          <th scope="row" className="px-6 py-4 font-medium   w-[173px]  ">
        MPM Pharma

         </th>
         <td className="px-6 py-4 text-[#050505] w-[163px] ">
           John Doe 
         </td>
          <td className="px-6 py-4  text-[#050505]">
            #12
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            29-01-24
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 1245
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            0.00
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 10
          </td>
          <td className="px-6 py-4  text-[#3C3C3C] font-[600]">
          &#8377; 1255
          </td>
          <td className="px-5 py-4 ]">
            <div className='w-[95px] bg-[#FFBEBE] px-[16px] py-[4px] rounded-[43px] max-sm:w-[85px]'>
              <h5 className='text-center font-[600] text-[14px] text-[#A8422B] max-sm:text-[12px]'>Unpaid</h5>
            </div>
          </td>
          <td className="px-6 py-4  ">
          <div className=' w-[95px] flex justify-between max-sm:w-[85px] max-sm:ml-[5px] '>
              <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]"  type="button" onClick={()=>{setEdit(true)}} > border_color </span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button"> delete</span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button">download</span>
              </div>
          </td>
        </tr>
  
  
  
  
        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
        <th scope="row" className="px-6 py-4 font-medium   w-[173px]  ">
        MPM Pharma

         </th>
         <td className="px-6 py-4 text-[#050505] w-[163px] ">
           John Doe 
         </td>
          <td className="px-6 py-4  text-[#050505]">
            #12
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            29-01-24
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 1245
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            0.00
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 10
          </td>
          <td className="px-6 py-4  text-[#3C3C3C] font-[600]">
          &#8377; 1255
          </td>
          <td className="px-[28px] py-4  text-[#3C3C3C]">
            <div className='w-[65px] bg-[#DCFFC1] px-[16px] py-[4px] rounded-[43px]'>
              <h5 className='text-center font-[600] text-[14px] text-[#01570F] max-sm:text-[12px]'>Paid</h5>
            </div>
          </td>
          <td className="px-6 py-4  ">
          <div className=' w-[95px] flex justify-between max-sm:w-[85px] max-sm:ml-[5px] '>
              <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]"  type="button" onClick={()=>{setEdit(true)}} > border_color </span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button"> delete</span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button">download</span>
              </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  
  
  
  
       </div>
       {/* ---- All List table  end------------- */}


          </div>
        )}
        {activeButton === 'btn2' && (
          <div>


            {/* ---- Outstanding table------------- */}
     <div className= ' bottom-div-with-hidden-scrollbar h-auto  mt-[24px]  overflow-x-auto' >
  

  <div className="relative ">
    <table className="w-full text-sm text-left rtl:text-right">
      <thead className="bg-[#FFFAEE] text-[#393939] text-[14px] font-[500] h-[67px] leading-[16.94px] max-sm:text-[12px]">
        <tr>
          <th scope="col" className="px-6 py-3">
            Client name
          </th>
          <th scope="col" className="px-6 py-3">
            Rep Name
          </th>
          <th scope="col" className="px-6 py-3">
            Invoice No:
          </th>
          <th scope="col" className="px-6 py-3">
            Invoice Date
          </th>
          <th scope="col" className="px-6 py-3">
            PTR amount
          </th>
          <th scope="col" className="px-6 py-3">
            Trade Disc
          </th>
          <th scope="col" className="px-6 py-3">
            GST + Cess
          </th>
          <th scope="col" className="px-6 py-3">
          Total
          </th>
          <th scope="col" className="px-6 py-3">
          Outstanding
          </th>
          <th scope="col" className="px-[45px] py-3">
          Action
          </th>
        </tr>
      </thead>
      <tbody className='h-[70px] font-[500] text-[16px] leading-[19.36px] text-[#464646] max-sm:text-[12px]'  style={{fontFamily:'Montserrat, sans-serif'}}>

        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 ">
        <th scope="row" className="px-6 py-4 font-medium   w-[173px]  ">
        MPM Pharma

         </th>
         <td className="px-6 py-4 text-[#050505] w-[163px] ">
           John Doe  mark
         </td>
          <td className="px-6 py-4  text-[#050505]">
            #12
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            29-01-24
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 1245
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            0.00
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 10
          </td>
          <td className="px-6 py-4  text-[#3C3C3C] font-[600]">
          &#8377; 1255
          </td>
          <td className="px-5 py-4 ]">
          <div className='w-[95px]  px-[16px] py-[4px] rounded-[43px] max-sm:w-[85px]'>
           
           <h5 className='text-center font-[600] text-[14px] text-[#01570F] max-sm:text-[12px]'>5 days</h5>
         </div>
          </td>
  
           <td className="px-6 py-4  ">
           <div className=' w-[95px] flex justify-between max-sm:w-[85px] max-sm:ml-[5px] '>
              <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]"  type="button" onClick={()=>{setEdit(true)}} > border_color </span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button"> delete</span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button">download</span>
              </div>
          </td>
  
        </tr>
  
          <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
          <th scope="row" className="px-6 py-4 font-medium   w-[173px]  ">
        MPM Pharma 

         </th>
         <td className="px-6 py-4 text-[#050505] w-[163px] ">
           John Doe 
         </td>
          <td className="px-6 py-4  text-[#050505]">
            #12
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            29-01-24
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 1245
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
            0.00
          </td>
          <td className="px-6 py-4  text-[#3C3C3C]">
          &#8377; 10
          </td>
          <td className="px-6 py-4  text-[#3C3C3C] font-[600]">
          &#8377; 1255
          </td>
          <td className="px-5 py-4 ]">
          <div className='w-[95px] px-[16px] py-[4px] rounded-[43px] max-sm:w-[85px]'>
              <h5 className='text-center font-[600] text-[14px] text-[#E32B1F] max-sm:text-[12px]'>Overdue</h5>
            </div>
          </td>
          <td className="px-6 py-4  ">
          <div className=' w-[95px] flex justify-between max-sm:w-[85px] max-sm:ml-[5px] '>
              <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]"  type="button" onClick={()=>{setEdit(true)}} > border_color </span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button"> delete</span>
            <span className="material-symbols-outlined font-[300]  cursor-pointer max-sm:text-[22px]" type="button">download</span>
              </div>
          </td>
        </tr>
  
  
  
  
      
      </tbody>
    </table>
  </div>
  
  
  
  
       </div>
       {/* ---- Outstanding table  end------------- */}


          </div>
        )}
      </div>
</div>
  
   </div>


   </div>
   </div>


   <div>

  {/* --calender modal */}
   <Modal onCancel = {()=> setShowCalender(false)} 
    footer={null} 
    visible={showCalender}
    width={'310px'}
    
    top={0}>
   
     {showModal && (
        <div className="modal pt-[30px] pl-[10px] ">
          <div className="modal-content">
            <DatePicker
              selected={startDate}
              onChange={handleDateSelect}
              selectsStart
              startDate={startDate}
              endDate={endDate}
              inline
            />
          </div>
          <div className='flex justify-between mt-[5px] px-1'>
            <button onClick={handleModalClose}>Cancel</button>
            <button className='pr-[12px]' onClick={handleOkButtonClick}>OK</button>
          </div>
        </div>
      )}
    </Modal>

    
    {/* --calender modal end--------- */}  


    {/* -----upload new modal---- */}
          
            <Modal onCancel = {()=> setVisible(false)} 
    footer={null} 
    visible={visible}
    width={'438px'}
    top={0}>

    <div className='pt-[20px] py-[24px]'style={{fontFamily:'Montserrat, sans-serif'}}>
        <div className='w-auto '>
            <h1 className='font-[600] text-[20px] leading-[24.38px] text-[#1C1C1C] mb-[24px]'>Add Invoice Details</h1>

            {/* --------form------------ */}
            <div className='w-auto '>
            <form >
                    <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            District
                        </label>
                        <div className='bg-[#FAFAFA]  border border-[#E3E3E3]  rounded w-full h-[40px] py-2 pl-2 pr-2'>
                          <select id="paid"  className=" bg-[#FAFAFA]  w-full   text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  >
                         <option  value="none" selected disabled hidden>Select District</option>

                            <option >Name1</option>
                            <option >Name2</option>
                           
                          </select>
                          </div>
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Client name
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Representative name
                        </label>
                        <div className='bg-[#FAFAFA]  border border-[#E3E3E3]  rounded w-full h-[40px] py-2 pl-2 pr-2'>
                          <select id="paid"  className=" bg-[#FAFAFA]  w-full   text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  >
                         <option  value="none" selected disabled hidden>Select Rep Name</option>

                            <option >Name1</option>
                            <option >Name2</option>
                           
                          </select>
                          </div>
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                          Invoice No
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Invoice Date
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            PTR Amount
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Trade Discount
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            GST + Cess
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Total
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Bill Status
                        </label>
                        <div className='bg-[#FAFAFA]  border border-[#E3E3E3]  rounded w-full h-[40px] py-2 pl-2 pr-2'>
                          <select id="paid"  className=" bg-[#FAFAFA]  w-full   text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  >
                         <option  value="none" selected disabled hidden>Choose Bill Status</option>

                            <option >Paid</option>
                            <option >Unpaid</option>
                           
                          </select>
                          </div>
                         

                        </div>

                        <div className='mt-[40px]  h-[49px] flex items-center relative'>
                        <label htmlFor="upload" className="font-[500] text-[16px] leading-[19.5px] text-[#A97604] max-sm:text-[12px] cursor-pointer">
                                    {fileName ? fileName : 'Upload Bill'}
                                  </label>
                                  <input id="upload" type="file" className="hidden" onChange={handleFileChange} />
                            {/* <h4 className='font-[500] text-[16px] leading-[19.5px] text-[#A97604] max-sm:text-[12px]'>Upload Bill</h4> */}
                            <div className='absolute right-0 w-[203] h-[49px]   '>
                            <button className="bg-[#ffff] hover:bg-[#fcfaf7] text-black font-[400] text-[12px] py-4 px-4 border border-[#EBEBEB] rounded mr-3 mt-0 h-[49px] w-[75px]  ">Cancel</button>
                           
                           
                            <button className="bg-[#F4CF7B] hover:bg-[#e7ca88] text-[#1A1919] font-[600] text-[14px] py-2 px-4 border border-[#F4CF7B] w-[105px] h-[48px] rounded ">Save</button>


                            </div>
                        </div>
 
                    </form>
            </div>

            {/* --------form  end------------ */}
        </div>
    </div>
    
    
    </Modal>
 {/* -----upload new modal end ---- */}   


    {/* -----edit table modal---- */}
          
    <Modal onCancel = {()=> setEdit(false)} 
    footer={null} 
    visible={edit}
    width={'438px'}
    top={0}>

    <div className='pt-[20px] py-[24px]'style={{fontFamily:'Montserrat, sans-serif'}}>
        <div className='w-auto '>
            <h1 className='font-[600] text-[20px] leading-[24.38px] text-[#1C1C1C] mb-[24px]'>Edit Invoice Details</h1>

            {/* --------form------------ */}
            <div className='w-auto '>
            <form >
                    <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            District
                        </label>
                        <div className='bg-[#FAFAFA]  border border-[#E3E3E3]  rounded w-full h-[40px] py-2 pl-2 pr-2'>
                          <select id="paid"  className=" bg-[#FAFAFA]  w-full   text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  >
                         <option  value="none" selected disabled hidden>Select District</option>

                            <option >Name1</option>
                            <option >Name2</option>
                           
                          </select>
                          </div>
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Client name
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Representative name
                        </label>
                        <div className='bg-[#FAFAFA]  border border-[#E3E3E3]  rounded w-full h-[40px] py-2 pl-2 pr-2'>
                          <select id="paid"  className=" bg-[#FAFAFA]  w-full   text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  >
                         <option  value="none" selected disabled hidden>Select Rep Name</option>

                            <option >Name1</option>
                            <option >Name2</option>
                           
                          </select>
                          </div>
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                          Invoice No
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Invoice Date
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            PTR Amount
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Trade Discount
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            GST + Cess
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Total
                        </label>
                        <input className=" border border-[#DCDCE4] rounded w-full h-[40px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text"  />
                        </div>
                        <div className="mb-4">
                        <label className="block text-[#32324D] text-[12px] font-[400] leading-[16px] mb-2" >
                            Bill Status
                        </label>
                        <div className='bg-[#FAFAFA]  border border-[#E3E3E3]  rounded w-full h-[40px] py-2 pl-2 pr-2'>
                          <select id="paid"  className=" bg-[#FAFAFA]  w-full  text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  >
                         <option  value="none" selected disabled hidden>Choose Bill Status</option>

                            <option >Paid</option>
                            <option >Unpaid</option>
                           
                          </select>
                          </div>

                        </div>

                        <div className='mt-[40px]  h-[49px] flex items-center relative'>
                                  <label htmlFor="edit" className="font-[500] text-[16px] leading-[19.5px] text-[#A97604] max-sm:text-[12px] cursor-pointer ">
                                    {fileName ? fileName : 'Edit Bill'}
                                  </label>
                                  <input id="edit" type="file" className="hidden" onChange={handleFileChange} />
                            {/* <h4 className='font-[500] text-[16px] leading-[19.5px] text-[#A97604] max-sm:text-[12px]'>Upload Bill</h4> */}
                            <div className='absolute right-0 w-[203] h-[49px]   '>
                            <button className="bg-[#ffff] hover:bg-[#fcfaf7] text-black font-[400] text-[12px] py-4 px-4 border border-[#EBEBEB] rounded mr-3 mt-0 h-[49px] w-[75px]  ">Cancel</button>
                           
                           
                            <button className="bg-[#F4CF7B] hover:bg-[#e7ca88] text-[#1A1919] font-[600] text-[14px] py-2 px-4 border border-[#F4CF7B] w-[135px] h-[48px] rounded ">Save change</button>


                            </div>
                        </div>
 
                    </form>
            </div>

            {/* --------form  end------------ */}
        </div>
    </div>
    
    
    </Modal>
 {/* -----edit table modal end ---- */}   


 


        </div>

    </div>
  )
}

export default Admin_panel
